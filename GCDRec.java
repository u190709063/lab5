//x = B.y + K
public class GCDRec {
    public static void main(String[] args) {
        int x = Integer.parseInt(args[0]);
        int y = Integer.parseInt(args[1]);
        System.out.println(GCD(x,y));
    }
    public static int GCD(int x,int y){
        if (x == 0){
            return y;
        }
        return GCD(y % x,x);
    }
}


